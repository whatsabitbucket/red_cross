require 'rubygems'
require 'bundler/setup'

require 'pry'

require 'logger'

require 'twilio-ruby'

Dir['./lib/**/*.rb'].sort.each { |file| require file }

logger = Logger.new(STDOUT)
logger.level = ENV.fetch('LOG_LEVEL') { :debug }.to_sym

# Lookup API
TWILIO_ACCOUNT_SID = ENV['TWILIO_ACCOUNT_SID']
TWILIO_AUTH_TOKEN = ENV['TWILIO_AUTH_TOKEN']

# Handle exit
at_exit do
  # logger.debug { 'Phone number lookup completed.' }
end

# Output to STDOUT
begin
  response = PhoneNumber.new(phone_numbers = ARGV, logger: logger)
  output = response.parse_nummbers
  $stdout.puts(output)

rescue StandardError => e
  logger.error(error: 'An unexpected error occurred', message: e.message, backtrace: e.backtrace)
end
