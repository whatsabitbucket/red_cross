class PhoneNumber

  attr_reader :logger
  attr_accessor :client, :sanitized_results, :phone_numbers

  def initialize(phone_numbers, logger: Logger.new(STDOUT))
    @phone_numbers = phone_numbers
    @logger = logger
    @client = Twilio::REST::Client.new(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN)
    @sanitized_results = []
  end

  # @return [String] STDOUT
  def parse_nummbers
    phone_numbers.each do |number|
      sanitized_results << country_code(number) + ' ' + sanitized(number)
    end
    sanitized_results
  end

  private

  # @param type [String]
  # @return [String]
  def country_code(number)
    client.lookups.phone_numbers(number).fetch.country_code
  end

  # @param type [String]
  # @return [String]
  def sanitized(number)
    client.lookups.phone_numbers(number).fetch.phone_number
  end

end
